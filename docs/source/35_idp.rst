..  _hci_permission:

Identity Provider Configuration
===============================

Introduced with **hcisle** v0.1.7, the tools needs to make a MAPI call to HCI to figure out which
eligible services are running. Due to that, a user is needed with permission granted for a tiny bit
of MAPI functionality needed. As the tool's execution is typically controlled by *systemd*, the user's
credentials need to be available in the :ref:`systemd unit file <autostart>`.

For security sake, the suggestion is to configure a (technical) user account who just has the bare minimum
permission granted:

*   HCI must be integrated with an Identity Provider (IDP)
*   The IDP should have

    *   a dedicated *Group* configured for the tool to use (ex.: ``hcislequery``)

    *   a dedicated (technical) *User* being member of the mentioned *Group*. Note that this *User* doesn't need
        (and shouldn't have) any other rights than the permission to log in to the IDP (*in Active Directory,*
        *it has been seen to be enough to allow the user to log in to the Domain, w/o any other*
        *permissions* assigned)

*   In the HCI Admin App, navigate to *Configuration > Security > Roles* and create a *role* (ex.: ``hcisle``)

    *   from Permission Group ``Admin Services``, enable ``admin:services:query:create``

*   then, navigate to *Configuration > Security > Groups* and create a new *Group* by
    discovering the *Group* prepared in the IDP (ex.: ``hcislequery``) and assign the previously created *Role*
    (ex.: ``hcisle``) to it.

Having that, the tool should be able to work with the *User* created in the IDP.
