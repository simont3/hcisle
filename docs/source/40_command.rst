Command syntax
==============

Environment variables
---------------------

Normally, the tool should be run periodically by a ``systemd timer``. If it should be necessary to run
the tool manually, make sure that there environment variables are set properly:

+----------------------+--------------------------------------------------------------------------------------+
| Environment variable | Description                                                                          |
+======================+======================================================================================+
| HCISLE_HOST          | hostname or IP address of one of the HCI instance, normally 'localhost'              |
+----------------------+--------------------------------------------------------------------------------------+
| HCISLE_USER          | an user with sufficient permission                                                   |
|                      | (:ref:`see Identity Provider Configuration <hci_permission>`)                        |
+----------------------+--------------------------------------------------------------------------------------+
| HCISLE_PASSWORD      | the user's password                                                                  |
+----------------------+--------------------------------------------------------------------------------------+
| HCISLE_REALM         | the user's Security Realm                                                            |
+----------------------+--------------------------------------------------------------------------------------+

.. raw:: latex

    \clearpage

Command arguments
-----------------

..  Note::

    The command **must** run as ``root`` on a single inctance (no need or sense to run it on more
    than one), as just ``root`` is allowed to trigger a log package creation.

    Be aware that the settings described in the :ref:`HCI configuration chapter <hci config>`
    are necessary for the command to provide useable results.


..  code-block::

    % hcisle --help
    usage: hcisle [-h] [--version] [--logpackage LOGPACKAGE] [--vacuumdb DAYS]
                  [-i INSTALLDIR] [-d LOGDIR] [-D DB] [-L {info,debug}]
                  [--syslog-ip SYSLOG_IP] [--syslog-port SYSLOG_PORT]
                  [--syslog-prot {UDP,TCP}] [--syslog-facility SYSLOG_FACILITY]

    optional arguments:
      -h, --help            show this help message and exit
      --version             show program's version number and exit
      --logpackage LOGPACKAGE
                            Optional HCI log package (instead of collecting logs from HCI)
      --vacuumdb DAYS       DB maintenance: remove sent records older than DAYS from the
                            database and VACUUM it afterwards (defaults to 31 days)
      -i INSTALLDIR         HCI installation folder (default: /opt/hci)
      -d LOGDIR             Folder for collected logs (default: /opt/hcisle)
      -D DB                 Database to store the logs
                            (default: /opt/hcisle/log.db)
      -L {info,debug}       Logging level
      --syslog-ip IP        Syslog server IP address
      --syslog-port PORT    Syslog server port (defaults to 514)
      --syslog-prot {UDP,TCP}
                            Syslog server protocol (defaults to UDP)
      --syslog-facility FACILITY
                            Syslog facility (defaults to "local0")


..  Note::

    *   The directories *must* exist before running **hcisle**!
    *   **hcisle** collects the last 24 hours of logs - so, the tool
        should run at least once a day.

Running the command ends up with a *sqlite3* database file created (or updated, on subsequent
runs) holding records from users activity in the *Search App*, as well as queries directed to
the Solr instances holding the respective Index(es).

If a SYSLOG-IP address has been given as a parameter, all new records will be sent to the
specified syslog server.

Log records are stored in the database with a flag that indicates if the record has already been sent
to a syslog server. Log records that have been sent will be removed from the database in a later run,
once ``--VACUUMDB days`` have passed.

.. raw:: latex

    \clearpage

A sample run
------------

..  code-block::

    (.venv) [root@hci206 hcisle]# hcisle --syslog-ip services.dc.snomis --syslog-facility local6
    2022-02-14 11:53:22,426 : hcisle v.0.1.5 (2022-02-14/Sm)
    2022-02-14 11:53:22,677 : collecting logs
    2022-02-14 11:53:36,442 : log package is /opt/hcisle/logs_1644836014972.zip
    2022-02-14 11:53:36,443 : log package: /opt/hcisle/logs_1644836014972.zip
    2022-02-14 11:53:36,452 : working on HCI: 483c4e55-1a8b-41f2-8d47-f960993f1573
    2022-02-14 11:53:36,452 : log package covering 2022-02-14 to 2022-02-14
    2022-02-14 11:53:36,452 : nodes in the log package: 192.168.0.206, 192.168.0.207, 192.168.0.208, 192.168.0.209
    2022-02-14 11:53:36,453 : processing node 192.168.0.206
    2022-02-14 11:53:36,524 : processing node 192.168.0.207
    2022-02-14 11:53:37,006 : processing node 192.168.0.208
    2022-02-14 11:53:37,388 : processing node 192.168.0.209
    2022-02-14 11:53:38,439 : log records found: 16 (searchapp), 10 (solrquery)
    2022-02-14 11:53:38,439 : thereof ignored (duplicate) log records: 1 (searchapp), 0 (solrquery)
    2022-02-14 11:53:39,296 : records sent to syslog: 25
    2022-02-14 11:53:39,296 : now removing records sent to syslog prior to Fri Jan 14 11:53:39 2022
    2022-02-14 11:53:39,296 : removed 0 records from the searchapp table
    2022-02-14 11:53:39,296 : removed 0 records from the solrquery table
    2022-02-14 11:53:39,296 : now running DB VACUUM
    2022-02-14 11:53:39,339 : DB VACUUM done
    2022-02-14 11:53:39,340 : cleaned up log package /opt/hcisle/logs_1644836014972.zip