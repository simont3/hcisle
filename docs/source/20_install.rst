..  _install:

Installation and Update
=======================

Installation
------------

As **hcisle** needs to run as ``root`` to be able to request an
HCI log package being created, the following should be run by ``root`` as well.

**Outside (!!!)** of HCIs installation directory, create a folder for **hcisle**.

Example: if HCI is installed into ``/opt/hci``, a folder in ``/opt`` might be
sufficient:

    ..  code-block::

        # mkdir /opt/hcisle
        # cd /opt/hcisle

Within that folder, create a Python3 virtual environment, activate and update necessary
packages. Then install **hcisle**:

    ..  code-block::

        # python3 -m venv .venv
        # source .venv/bin/activate
        (.venv) # pip install -U pip setuptools wheels  # optional!
        (.venv) # pip install hcisle

    ..  Tip::

        In case your HCI instance doesn't have internet access, you can also download the latest **hcisle** package
        using any other computer with Internet access from the `Python Package index <https://pypi.org/project/hcisle/>`_.
        In that case, transfer the downloaded package to the HCI instance and run the install this way:

            ..  code-block::

                (.venv) # pip install hcisle-<version>.tar.gz



Test the installation by running:

    ..  code-block::

        (.venv) # hcisle --version

..  Note::

    Remember that every time the tool shall be run manually (that is, not by ``systemd``),
    the Python virtual environment needs to be activated first!

Update
------

When a new version of **hcisle** gets available, install it this way:

Change to the **hcisle**'s installation directory and enable the Python virtual environment:

    ..  code-block::

        # cd /opt/hcisle
        # source .venv/bin/activate
        (.venv) # pip install -U hcisle

    ..  Tip::

        In case your HCI instance doesn't have internet access, you can also download the latest **hcisle** package
        using any other computer with Internet access from the `Python Package index <https://pypi.org/project/hcisle/>`_.
        In that case, transfer the downloaded package to the HCI instance and run the install this way:

            ..  code-block::

                (.venv) # pip install -U hcisle-<new_version>.tar.gz


After a Python upgrade
----------------------

Sometimes a virtual environment gets unstable when the Python installation itself
was updated.

To fix that:

*   de-activate the environment -just in case it is active- by running

    ..  code-block::

        (.venv) # deactivate

*   Delete the ``.venv`` folder

*   Follow the :ref:`Installation <install>` steps to re-create the virtual environment and to
    re-install **hcisle**. There is no need to change the :ref:`systemd unit file <autostart>`,
    btw.
