..  _autostart:

Setup for a scheduled run
=========================

..  Note::

    As the tool just needs to run on a single instance, it is enough to install the service and
    timer units described below **on just one instance**.

A ``systemd timer`` should be used to run **hcisle** periodically, every few hours.

Therefore, ``systemd unit files`` for a service and a timer needs to be created.

..  Note::

    Paths used in the unit files need to be adopted to match the installation folder,
    :ref:`see the installation chapter <install>`.


**/etc/systemd/system/hcisle.service**

    ..  code-block::

        [Unit]
        Description=HCI searchlog extractor
        Wants=hcisle.timer

        [Service]
        Type=oneshot
        WorkingDirectory=/opt/hcisle
        Environment="HCISLE_HOST=localhost"
        Environment="HCISLE_USER=techuser"
        Environment="HCISLE_PASSWORD=techuserspassword"
        Environment="HCISLE_REALM=Local"
        ExecStart=/opt/hcisle/.venv/bin/python /opt/hcisle/.venv/bin/hcisle -i /opt/hci -d /opt/hcisle -D /opt/hcisle/log.db -L info --syslog-ip 192.168.0.2 --syslog-port 514 --syslog-prot UDP --syslog-facility local6 --vacuumdb 31

        [Install]
        WantedBy=multi-user.target

**/etc/systemd/system/hcisle.timer**

    ..  code-block::

        [Unit]
        Description=Runs HCI searchlog extractor service
        Requires=hcisle.service

        [Timer]
        Unit=hcisle.service
        # start the service hourly
        OnCalendar=hourly

        [Install]
        WantedBy=timers.target

Once the unit files are in place, activate the timer by running:

..  code-block::

    # systemctl enable --now hcisle.timer

Check when it will run the next time:

..  code-block::

    # systemctl list-timers
