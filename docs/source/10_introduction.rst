Introduction
============

Some operators of *Hitachi Content Intelligence* (HCI) require the ability to follow
up with search users activity, in the form of audit logging.

**hcisle** is a tool that is able to collect relevant log records written by the
*Search App* and the *Solr* instances (forming the indexes in HCI) whenever a user
submits a search request to HCI.

As a requirement, *Solr* logging must be configured so that it creates the wanted
records. The :ref:`HCI Logging Configuration chapter <hci config>` explains how to do that.

As **hcisle** is relying on log packages created by HCI, it should be
scheduled to run periodically, at least once a day.

The :ref:`Code Description chapter <code>` gives an overview about how the code works.

**hcisle** doesn't have any dependencies beside a pure Python 3
installation. Great care was taken to make sure that the tool doesn't cause any
security breaches. Nevertheless:

..  Tip::

    As HCI is often used in security critical context (and the tool needs to run as ``root``),
    you are **highly encouraged** to do your own code review before using it in such a context.
    See the :ref:`Code Description chapter <code>` to learn about the code structure.

