Prerequisites
=============

*   An Hitachi Content Intelligence (HCI) installation
*   An user with  :ref:`permission to the HCI Management API <hci_permission>`
*   **hcisle** needs to run on one of the HCI instances
    (while it doesn't really matter on which one)
*   Python 3.6 or higher needs to be available on the HCI instance used